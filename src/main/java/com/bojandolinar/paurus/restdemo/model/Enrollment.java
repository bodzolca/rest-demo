package com.bojandolinar.paurus.restdemo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@Entity
@Table(uniqueConstraints =
         @UniqueConstraint(name = "UC_ENROLL_STUDENT_COURSE",
                           columnNames = {"student_id", "course_id"}))
public class Enrollment {
    private @Id @GeneratedValue
    Long id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "student_id")
    private Student student;
    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    public
    Enrollment() {
    }

    public Enrollment(Student student, Course course) {
        this.student = student;
        this.course = course;
    }
}
