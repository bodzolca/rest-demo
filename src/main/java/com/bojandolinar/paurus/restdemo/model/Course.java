package com.bojandolinar.paurus.restdemo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Course {
    private @Id
    @GeneratedValue
    Long id;
    private String name;

    public Course() {
    }

    public Course(String name) {
        this.name = name;
    }
}
