package com.bojandolinar.paurus.restdemo.repository;

import com.bojandolinar.paurus.restdemo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository extends JpaRepository<Student, Long> {
    Student findByUsername(String username);
}
