package com.bojandolinar.paurus.restdemo.repository;

import com.bojandolinar.paurus.restdemo.model.Course;
import com.bojandolinar.paurus.restdemo.model.Enrollment;
import com.bojandolinar.paurus.restdemo.model.Student;
import com.bojandolinar.paurus.restdemo.repository.CourseRepository;
import com.bojandolinar.paurus.restdemo.repository.EnrollmentRepository;
import com.bojandolinar.paurus.restdemo.repository.StudentsRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Random;

@Configuration
@Slf4j
public class DatabaseInitLoader {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    CommandLineRunner initCourses(CourseRepository repo) {
        return args -> {
            repo.save(new Course("math"));
            repo.save(new Course("chemistry"));
            repo.save(new Course("history"));
            repo.save(new Course("physics"));
            repo.save(new Course("physiology"));
            repo.save(new Course("biology"));
        };
    }

    @Bean
    CommandLineRunner initStudents(StudentsRepository repo) {
        return args -> {
            repo.save(new Student("Gordon", "Shumway", "gordons", passwordEncoder.encode("gordons")));
            repo.save(new Student("Willie", "Tanner", "williet", passwordEncoder.encode("williet")));
            repo.save(new Student("Martha", "Wayne", "marthaw", passwordEncoder.encode("marthaw")));
        };
    }

    // creates a deterministic subset of all possible enrollments
    @Bean
    CommandLineRunner initEnrollments(EnrollmentRepository repo,
                                      StudentsRepository studentRepo,
                                      CourseRepository courseRepo
                                      ) {
        return args -> {
            StreamEx<Boolean> fifthTrue =
                    IntStreamEx.of(new Random(10)) // fixed seed so that enrollments are deterministic
                                .mapToObj(it -> it % 5 == 0);

            StreamEx.of(studentRepo.findAll()).cross(courseRepo.findAll())
                    .mapKeyValue(Enrollment::new)
                    .zipWith(fifthTrue)
                    .filterValues(Boolean::valueOf).keys()
                    .forEach(repo::save);
        };
    }
}
