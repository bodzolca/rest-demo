package com.bojandolinar.paurus.restdemo.repository;

import com.bojandolinar.paurus.restdemo.model.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
}
