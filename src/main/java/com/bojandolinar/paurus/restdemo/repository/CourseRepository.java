package com.bojandolinar.paurus.restdemo.repository;

import com.bojandolinar.paurus.restdemo.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
