package com.bojandolinar.paurus.restdemo.controller;

import com.bojandolinar.paurus.restdemo.model.Course;
import com.bojandolinar.paurus.restdemo.repository.CourseRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/courses")
@Slf4j
public class CoursesController {
    @Autowired
    private CourseRepository repo;

    @GetMapping
    public List<Course> getAllCourses(@AuthenticationPrincipal UserDetails userDetails) {
        log.info("Show all courses");
        return repo.findAll();
    }

    @GetMapping("/{name}")
    public List<Course> getCoursesByName(@PathVariable("name") String name) {
        System.out.println("CoursesController.getCoursesByName: " + name);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();
        return repo.findAll(Example.of(new Course(name), matcher));
    }
}
