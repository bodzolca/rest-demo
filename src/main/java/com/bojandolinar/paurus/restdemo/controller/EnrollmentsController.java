package com.bojandolinar.paurus.restdemo.controller;

import com.bojandolinar.paurus.restdemo.model.Enrollment;
import com.bojandolinar.paurus.restdemo.repository.EnrollmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/enrollments")
@Slf4j
public class EnrollmentsController {
    @Autowired
    private EnrollmentRepository repo;

    @GetMapping
    public List<Enrollment> getAllEnrollments() {
        return repo.findAll();
    }

    @PostMapping
    public ResponseEntity<Enrollment> enrollStudent(@AuthenticationPrincipal UserDetails userDetails,
                                                    @RequestBody Enrollment enrollment) {
        // todo: link UserDetails to student and use it as a parameter (overriding the one in enrollment)
        log.info("create enrollment {}", enrollment);
        repo.save(enrollment);
        return new ResponseEntity<>(enrollment, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public HttpStatus cancelEnrollmentById(@AuthenticationPrincipal UserDetails userDetails,
                                           @PathVariable("id") Long id) {

        // todo: link UserDetails to student and check if his enrollment
        log.info("Cancel enrollment id = {}", id);
        repo.deleteById(id);
        return HttpStatus.OK;
    }
}
