package com.bojandolinar.paurus.restdemo.controller;

import com.bojandolinar.paurus.restdemo.model.Student;
import com.bojandolinar.paurus.restdemo.repository.StudentsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students")
@Slf4j
public class StudentController {
    @Autowired
    private StudentsRepository studentsRepository;

    @GetMapping
    public List<Student> getAllStudents() {
        List<Student> all = studentsRepository.findAll();
        log.info("Returning all {} students", all.size());
        return all;
    }
}
