package com.bojandolinar.paurus.restdemo;

import com.bojandolinar.paurus.restdemo.model.Student;
import com.bojandolinar.paurus.restdemo.repository.StudentsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DemoUserDetailsService implements UserDetailsService {
    @Autowired
    private StudentsRepository studentsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user by username '{}'", username);
        Student student = studentsRepository.findByUsername(username);
        if (student == null) {
            throw new UsernameNotFoundException(username);
        }

        log.info("Password for username {}", student.getPassword());
        return User.withUsername(username).password(student.getPassword()).roles("STUDENT").build();
    }
}
