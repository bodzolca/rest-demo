package com.bojandolinar.paurus.restdemo;

import com.bojandolinar.paurus.restdemo.repository.DatabaseInitLoader;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * <p>Tests the data filled with {@link DatabaseInitLoader}.</p>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RestDemoApplicationTests {
	@Autowired
	private TestRestTemplate template;

	@LocalServerPort
	private int port;

	@Test
	@Order(10)
	public void shouldReturnAllCourses() throws Exception {
		List response = template.withBasicAuth("gordons", "gordons")
				.getForObject("http://localhost:" + port + "/courses", List.class);

		assertThat(response)
				.asList().hasSize(6)
				.extracting("name")
				.containsExactlyInAnyOrder("math", "chemistry", "history", "physics", "physiology", "biology");
	}

	@Test
	@Order(20)
	public void shouldReturnAllStudents() throws Exception {
		List response = template.withBasicAuth("gordons", "gordons")
				.getForObject("http://localhost:" + port + "/students", List.class);

		assertThat(response)
				.asList().hasSize(3)
		        .extracting("username")
		.containsExactlyInAnyOrder("gordons", "williet", "marthaw");
	}

	@Test
	@Order(30)
	public void shouldReturnAllEnrollments() throws Exception {
		List response = template.withBasicAuth("gordons", "gordons")
				.getForObject("http://localhost:" + port + "/enrollments", List.class);

		assertThat(response)
				.asList().hasSize(5);
	}

	// todo: test creating and deleting enrollment.
	// With these tests ordering of methods will be important. Right now they are not.

}
