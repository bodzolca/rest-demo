Introduction
---

Simple REST services for class enrollments that was done as a job interview challenge.

I just skimmed the testing part because of some constant glitches there, suspecting that
TestRestTemplate is not as comprehensive as I would like it to be.

Building and running
---
```mvn spring-boot:run```

Some students, courses and enrollments are already filled in. See
DatabaseInitLoader.